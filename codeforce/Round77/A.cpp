#include <iostream>
#include <vector>
using namespace std;
void solve() {
    int n, m, x, c, r, result;
    cin >> m >> n;
    r = n % m;
    x = n / m;
    c = x * x;
    result = c * (m - r);
    x++;
    result += x * x * r;
    cout << result << '\n';
}
int main () {
    int t;
    cin >> t;
    for(int i = 0; i < t; i++) {
        solve();
    }
	return 0;
}