#include <iostream>
#include <vector>
#include <unordered_map>
using namespace std;

void solve() {
    int n,coutn = 0;
    string tmp;
    cin >> n;
    vector<string> pin, pon;
    vector<vector<bool> > used(4, vector<bool> (10, 0));
    unordered_map<string, int> map;
    vector<bool> usdidx(n, 0);
    for (int i = 0; i < n; i++) {
        cin >> tmp;
        pin.push_back(tmp);
        // pon.push_back(tmp);
        if(map.find(tmp) == map.end()){
            map[tmp] = 1;
            // pin.pop_back();
            usdidx[i] = 1;
            for (int d = 0; d < 4; d++)
                used[d][int(tmp[d] - '0')] = 1;
            
        }
    }

    for (int i = 0; i < pin.size(); i++) {
        if(usdidx[i]) continue;
        coutn++;
        for (int w = 0; w < 4; w++) {
            for(int h = 0; h < 10; h++){
                if(!used[w][h]) {
                    pin[i][w] = char(h + int('0'));
                    used[w][h] = 1;
                    w = 3;
                    break;
                }
            }
        }
    }
    cout << coutn << "\n";
    for(int i = 0; i < n; i++) {
        cout << pin[i] << "\n";
    }
}

int main () {
    int t;
    cin >> t;

    for(int i = 0; i < t; i++) {
        solve();
    }
    return 0;
}
