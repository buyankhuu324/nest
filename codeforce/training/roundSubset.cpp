#include <iostream>
#include <vector>
#include <algorithm>
#define ull unsigned long long
using namespace std;

pair<int, int> unpack(ull n) {
    int five = 0, two = 0;
    while(n % 5 == 0 || n % 2 == 0) {
        if(n % 5 == 0) {
            five++;
            n /= 5;
        }
        if(n % 2 == 0) {
            two++;
            n /= 2;
        }
    }
    return make_pair(five, two);
}

int main () {
    int n, k, sum = 0;
    cin >> n >> k;
    vector<vector<int> > dp1 , dp2;
    vector<pair<int, int> > kor(n);
    for(int i = 0; i < n; i++) {
        ull a;
        cin >> a;
        kor[i] = unpack(a);
        sum += kor[i].first;
    }
    dp1.resize(sum + 1, vector<int> (k, -1));
    dp2 = dp1;
    for(int i = 0; i < n; i++) {
        for(int j = 0; j < k; j++) {
            for(int l = 0; l <= sum; l++) {
                if(j == 0) {
                    if(l == kor[i].first) 
                        dp1[kor[i].first][0] = max(kor[i].second, dp2[kor[i].first][0]);
                    else dp1[l][j] = dp2[l][j];
                } else {
                    if(kor[i].first <= l && dp2[l - kor[i].first][j - 1] != -1)
                        dp1[l][j] = max(dp2[l - kor[i].first][j - 1] + kor[i].second, dp2[l][j]);
                    else dp1[l][j] = dp2[l][j];
                }
            }
        }
        dp2 = dp1;
    }
    int result = 0;
    for(int i = 0; i <= sum; i++)
        result = max(min(dp1[i][k - 1], i), result);
    cout << result << "\n";
    return 0;
}