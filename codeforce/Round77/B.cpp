#include <iostream>
#include <vector>
using namespace std;

void solve () {
    long long a, b;
    cin >> a >> b;
    if(a < b) {
        swap(a, b);
    }
    if(a > 2 * b) {
        cout << "no\n";
        return;
    }
    if((a + b) % 3 == 0) 
        cout << "yes\n";
    else cout << "no\n";
}

int main () {
    long long t;
    cin >> t;

    for(int i = 0; i < t; i++) {
        solve();
    }
    return 0;
}