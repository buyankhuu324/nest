#include <iostream>
#include <vector>
#include <cmath>
using namespace std;

int D(int a,int n) {
    if(n < 10) {
        if(a <= n) return 1;
        else return 0;
    }

    int temp = n, x = log10(n) + 1, count, num;
    count = pow(10, x - 1);

    if(n == count * 10 - 1) { 
        return x * count;
    }
    int first_digit = n / count;

    if(n % count == count - 1) {
        if(first_digit <= a) {
            int temp = (x- 1) * count - (10 - first_digit - 1) * (x - 1) * count / 10;
            return temp;
        } 
        int temp = (x) * count - (10 - first_digit - 1) * (x - 1) * count / 10;
        return temp;
    }
    int result = 0;
    if(first_digit == a) {
        int q = D(a, first_digit * count - 1), w = n % count + 1, e = D(a, n % count);
        result += q + w + e;
    } else if(first_digit < a) {
        int q = D(a, first_digit * count - 1), w = D(a, n % count);
        result += q;
        result += w;
    } else {
        int q = D(a, first_digit * count - 1), w = D(a, n % count); 
        result += q;
        result += w;
    }
    return result;
}

int count (int n) {
    int x = 0;
    int temp = n;
    while (temp != 0) {
        x ++;
        temp /= 10;
    }
    int result = 0;
    while (x > 0) {
        int count = pow(10, x - 1);
        result += (n - count + 1) * x;
        n = count - 1;
        x--;
    }
    return result;
}

int main() {
    int n, m, k;
    cin >> n >> m >> k;
    vector<int> d(10);
    if(count(m) - count(n - 1) <= k) {
        cout << -1 << "\n";
        return 0;
    }
    for(int i = 1; i < 10; i++) {
        d[i] = D(i, m)  - D(i, n - 1);
        cout << d[i] << "\n";
    }
    int sum = 0;
    for(int i = 9; i >= 1; i--) {
        sum += d[i];
        if(sum >= k) {
            cout << i << "\n";
            return 0;
        }
    }
    cout << 0 << '\n';
    return 0;
}