#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int solve () {
    int n;
    cin >> n;
    vector<int> ar(n);
    for (int i = 0; i < n; i++)
        cin >> ar[i];
    sort(ar.begin(), ar.end());
    int length = 1, index = n - 1;
    while (index >= 0 && length <= ar[index]) {
        length++;
        index--;
    }
    return length - 1;
}

int main () {
    int k;
    cin >> k;
    vector<int> results(k);
    for (int i = 0; i < k; i++)
        results[i] = solve();
    
    for (int i = 0; i < k; i++)
        cout << results[i] << "\n";

    return 0;
}