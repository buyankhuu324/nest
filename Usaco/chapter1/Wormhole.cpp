/*
ID: n.nekog1
LANG: C++
TASK: wormhole
*/
#include <iostream>
#include <fstream>
#include <vector>
#include <unordered_map>
#include <algorithm>

using namespace std;
struct hole{
    int x;
    int y;
};

int result = 0;
vector<hole> holes;
vector<int> prep(12);

void count(int n, unordered_map<int, int> portal) {
    for (int i = 0;  i < n; i++){
        if (prep[i] == - 1)
            continue;
        else{
            // cout << " here\n";
            vector<bool> check(n, 0);
            int j = i;
            while(prep[j] != -1 && check[j] == 0){
                check[j] = 1;
                j = portal[prep[j]];
            }
            if(check[j] == 1) {
                result++;
                break;
            }
        }
    }
}

void interruptions(vector<int> idx, vector<pair<int, int> > pairs){
    if(idx.size() == 2) {
        pairs.push_back(make_pair(idx[0], idx[1]));
        unordered_map<int, int> temp;
        for(int i = 0; i < pairs.size(); i++) {
            temp[pairs[i].first] = pairs[i].second;
            temp[pairs[i].second] = pairs[i].first;
            cout << pairs[i].first << " " << pairs[i].second << "\n";
        }
        count(pairs.size() * 2 , temp);
    } else {
        for(int i = idx.size() - 2; i >= 0; i--) {
            vector<pair<int, int> > tmp_pairs = pairs;
            vector<int> tmp = idx;
            tmp_pairs.push_back(make_pair(idx[idx.size() - 1], idx[i]));
            swap(tmp[i], tmp[tmp.size() - 2]);
            tmp.pop_back();
            tmp.pop_back();
            interruptions(tmp, tmp_pairs);
        }
    }
}

int main() {
    ifstream in ("wormhole.in");
    ofstream out ("wormhole.out");
    
    int n;
    in >> n;
    holes.resize(n);
    vector<int> temp;
    unordered_map<int, vector<int> > mymap;
    
    for(int i = 0; i < n; i++) {
        in >> holes[i].x >> holes[i].y;
        temp.push_back(i);
        mymap[holes[i].y].push_back(i);
    }
    for(unordered_map<int, vector<int> >::iterator it = mymap.begin(); it != mymap.end(); ++it) {
        sort(it->second.begin(), it->second.end());
        for(int i = 0; i < it->second.size(); i++) {
            if(i < it->second.size() - 1) {
                prep[it->second[i]] = it->second[i + 1];
            } else { 
                prep[it->second[i]] = -1;
            }
        }
    }
    vector<pair<int, int> > pairs;
    interruptions(temp, pairs);
    out << result << "\n";

    
    return 0;
}