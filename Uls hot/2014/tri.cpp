#include <iostream>
#include <vector>

using namespace std;

int a, b, c;

vector<int> find_digit(long long n) {
    vector<int> result;
    while(n != 0) {
        result.push_back(n % 10);
        n /= 10;
    }
    return result;
}

vector<int> make_numbers(int n, vector<int> &digit) {
    vector<int> result;
    if(n == 1) {
        for(int i = 0; i < digit.size(); i++) {
            result.push_back(digit[i]);    
        }
        return result;
    }
    vector<int> next = make_numbers(n - 1, digit);
    int k = 1;
    for(int i = 1; i < n; i++) 
        k *= 10;
    for(int i = 0; i < digit.size(); i++) {
        for(int j = 0; j < next.size(); j++)
            result.push_back(digit[i] * k + next[j]);
    }
    return result;
}

bool find (int k, vector<int> nums, vector<int> cur, long long n, vector<int> digit) {
    if(k == 3) {
        long long result = 1;
        for(int i = 0; i < 3; i++) {
            result *= nums[i];
        }
        if(result == n) {
            a = nums[0];
            b = nums[1];
            c = nums[2];
            return 1;
        }
        return 0;
    }
    vector<int> number = make_numbers(cur[k], digit);
    for(int i = 0; i < number.size(); i++) {
        vector<int> temp = nums;
        temp.push_back(number[i]);
        if(find(k + 1, temp, cur, n, digit))
            return 1;
    }
    return 0;
}

vector<vector<int> > make_cur(int k, int n) {
    vector<vector<int> > result;
    for(int i = 1; i <= n; i++) {
        vector<int> temp;
        temp.push_back(i);
        result.push_back(temp);
    }

    if(k == 3)
        return result;
    vector<vector<int> > prev = make_cur(k + 1, n);
    for(int j = 0; j < n; j++) {
        for(int i = 0; i < prev.size(); i++)
            result.push_back(prev[i]);
    }
    return result;
}

int main () {
    long long n;
    cin >> n;
    vector<int> digits = find_digit(n);
    for(int i = 0; i < digits.size(); i++) {
        if(digits[i] == 1) {
            cout << "1 1 " << n;
            return; 
        }
    }
    // vector<int> cur = make_cur(1, digits.size());
    // vector<int> nums;
    // for(int i = 0; i < cur.size(); i++) {
    //     if(find(1, nums, , n, digits)) {

    //     }
    // }

    return 0;
}