#include <iostream>
#include <vector>
#include <unordered_map>

using namespace std;

void solve(){
    unordered_map<int, int> position;
    int n;
    cin >> n;
    vector<int> nums(n);

    for(int i = 0; i < n; i++) {
        cin >> nums[i];
        position[nums[i]] = i;
    }
    vector<pair <int, int> > result(n + 1);
    int l = position[1], r = position[1];

    for (int i = 2; i <= n; i++) {
        result[i - 1] = make_pair(l, r);
        if (position[i] <= r && position[i] >= l) 
            continue;
        int cur = position[i];
        if (cur < l) 
            l = cur;
        else 
            r = cur;
    }
    result[n] = make_pair(l, r);
    
    for (int i = 1; i <= n; i++) {
        int x = result[i].first, y = result[i].second;
        if (y - x + 1 <= i)
            cout << 1;
        else 
            cout << 0;
    }
    cout << '\n';
}

int main(){
    int t;
    cin >> t;
    for(int i = 0; i < t; i++)
        solve();
    return 0;

}