/*
ID: n.nekog1
LANG: C++
TASK: skidesign
*/
#include <iostream>
#include <fstream>
#include <vector>

using namespace std;
int calc(vector<int> hills) {
    int best, price = 0;
    for(int i = 0; i <= 83; i++) {
        for(int j = 0; j < hills.size(); j++) {
            if(hills[j] < i)
                price += (i - hills[j]) * (i - hills[j]);
            if(hills[j] > i + 17)
                price += (hills[j] - i - 17) * (hills[j] - i - 17);
        }
        if(i == 0)
            best = price;
        best = min(best, price);
        price = 0;
    }
    return best;
}

int main() {
    ifstream in ("skidesign.in");
    ofstream out ("skidesign.out");
    
    int n;
    in >> n;

    vector<int> hills(n);
    for(int i = 0; i < n; i++) {
        in >> hills[i];
    }
    out << calc(hills) << "\n";
    return 0;
}