#include <iostream>
#include <stack>

using namespace std;

void rec(stack<int>& a) {
    if(a.empty())
        return;
        
    cout << a.top() << "\n";
    a.pop();
    rec(a);
}

int main() {
    int n, num;
    cin >> n;
    stack<int> a;

    for(int i = 0; i < n; i++) {
        cin >> num;
        a.push(num);
    }
    rec(a);

    return 0;
}