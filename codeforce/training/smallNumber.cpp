#include <iostream>
#include <vector>
#define ll long long
using namespace std;

ll calc (ll a, ll b, char c) {
    if(c == '+')
        return a + b;
    return a * b;
}

int main () {
    vector<int> nums(4);
    for(int i = 0; i < 4; i++) {
        cin >> nums[i];
    }
    vector<char> s(3);
    for(int i = 0; i < 3; i++) {
        cin >> s[i];
    }
    ll result = 1000000000000;
    for(int i = 0; i < 4; i++) {
        for(int j = i + 1; j < 4; j++) {
            ll temp = calc(nums[i], nums[j], s[0]);
            // cout << temp << "\n";
            vector<ll> rest;
            rest.push_back(temp);
            for(int l = 0; l < 4; l++) {
                if(l != i && l != j)
                    rest.push_back(nums[l]);
            }
            int q, w;
            for(q = 0; q < 3; q++) {
                for(w = q + 1; w < 3; w++) {
                    temp = calc(rest[q], rest[w], s[1]);
                    // cout << temp << "temp\n";
                    for(int e = 0; e < 3; e++) {
                        if(e != q && e != w){
                            temp = calc(rest[e], temp, s[2]);
                            result = min(result, temp);
                        }
                    }
                }
            }
            // cout << "-------------\n";
            // for(int e = 0; e < 3; e++) {
            //     if(e != q && e != w){
            //         temp = calc(rest[e], temp, s[2]);
            //         result = min(result, temp);
            //     }
            // }
        }
    }
    cout << result << "\n";
    return 0;
}