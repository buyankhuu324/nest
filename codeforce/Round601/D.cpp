#include <iostream>
#include <vector>

using namespace std;

char color[] = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','0','1','2','3','4','5','6','7','8','9'};

void solve () {
    int n, m, r, count = 0, forone, left, index = 0;
    cin >> n >> m >> r;
    vector<string> rice(n);
    for(int i = 0; i < n; i++) {
        cin >> rice[i];
    }
    for(int i = 0; i < n; i++) {
        for(int j = 0; j < m; j++) {
            if(rice[i][j] == 'R')
                count ++;
        }
    }
    vector<int> hed(r);
    forone = count / r;
    left = count % r;
    for(int i = 0; i < r; i++) {
        if(i < left) 
            hed[i] = forone + 1;
        else 
            hed[i] = forone;
    }
    for(int i = 0; i < n; i++) {
        if(i % 2 == 0){
            for(int j = 0; j < m; j++) {
                
                if(rice[i][j] == 'R') {
                    if(hed[index] == 0) {
                        index ++;
                    }
                    hed[index]--;
                }
                rice[i][j] = color[index];
            }
        }else{  
            for(int j = m - 1; j >= 0; j--) {
                if(rice[i][j] == 'R') {
                    if(hed[index] == 0) {
                        index ++;
                    }
                    hed[index]--;
                }
                rice[i][j] = color[index];
            }
        }
    }
    for(int i = 0; i < n; i++) {
        for(int j = 0; j < m; j++) {
            cout << rice[i][j];
        }
        cout << endl;
    }

}

int main () {
    int t;
    cin >> t;

    for(int i = 0; i < t; i++) {
        solve();
    }
    return 0;
}