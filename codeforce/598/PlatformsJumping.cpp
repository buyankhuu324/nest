#include <iostream>
#include <queue>
#include <vector>

using namespace std;

vector<int> path;

bool check(int n, int m, int d, queue<int> plathform, int standing){
    if(plathform.empty()) {
        if(standing + d >= n)
            return 1;
        return 0;
        // return 1;
    }
    if(standing >= n)
        return 0;
        
    bool best = 0;
    for (int i = 1; i <= d; i++) {
        queue<int> temp = plathform;
        temp.pop();
        best = check(n, m + 1, d, temp, standing + i + plathform.front() - 1);
        if(best) {
            for(int j = 0; j < plathform.front(); j++)
                path[standing + i + j] = m;
            return 1;
        }
    }
    return 0;
}

int main () {
    int n, m, d;
    int sum = 0;
    cin >> n >> m >> d;
    path.resize(n, 0);
    queue<int> plathform;
    for (int i = 0; i < m; i++) {
        int temp;
        cin >> temp;
        plathform.push(temp);
        sum += temp;
    }
    if(sum + (m + 1) * d < n)
        cout << "NO\n";
    else {
        bool result = check (n, 1, d, plathform, -1);
        if(result) {
            cout << "YES\n";
            for (int i = 0; i < n - 1; i++)
                cout << path[i] << " ";
            cout << path[n - 1] << "\n";
        }
    }

    return 0;
}