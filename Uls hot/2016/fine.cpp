#include <iostream>
#include <map>
#include <vector>
using namespace std;

int main () {
    int n, b;
    string a;
    cin >> n;
    map<int, vector<string> > fine;
    for(int i = 0; i < n; i++) {
        cin >> a >> b;
        fine[b].push_back(a);
    }
    map<int, vector<string> >::iterator it = fine.end();
    it--;
    sort(it->second.begin(), it->second.end());
    for(int i = it->second.size() - 1; i >= 0; i--) {
        cout << it->second[i] << ' ';
    }
    cout << '\n';
    return 0;
}