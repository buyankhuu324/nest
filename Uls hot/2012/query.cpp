#include <iostream>
#include <vector>

using namespace std;

struct SegmentTree {
    int len;
    vector<int> input;
    vector<pair<int, int> > tree;

    int left(int n) {
        return n * 2;
    }
    int right (int n) {
        return n * 2 + 1;
    }

    pair<int, int> rmq(int cur, int l, int r, int i, int j){
        if(j < l || i > r)
            return make_pair(-1, -1);
        if(i <= l && j >= r)
            return tree[cur];

        pair<int, int> li = rmq(left(cur), l, l + ( r - l) / 2, i , j);
        pair<int, int> ri = rmq(right(cur), l + (r - l) / 2 + 1, r, i , j);
        pair<int, int> result;
        
        if(li.first == -1)
            return ri;
        if(ri.first == -1)
            return li;

        if(input[li.first] > input[ri.first])
            result.first = li.first;
        else 
            result.first = ri.first;

        if(input[li.second] < input[ri.second])
            result.second = li.second;
        else 
            result.second = ri.second;
        return result;
    }

    pair<int, int> rmq (int i, int j) {
        return rmq(1, 0, len - 1, i , j);
    }

    void build(int cur, int l, int r) {
        if(l == r)
            tree[cur] = make_pair(l, l);
        else {
            build(left(cur), l, l + (r - l) / 2);
            build(right(cur), l + (r - l) / 2 + 1, r);

            if(input[tree[left(cur)].first] > input[tree[right(cur)].first]){
                tree[cur].first = tree[left(cur)].first;
            }else{
                tree[cur].first = tree[right(cur)].first;
            }

            if(input[tree[left(cur)].second] < input[tree[right(cur)].second]){
                tree[cur].second = tree[left(cur)].second;
            }else{
                tree[cur].second = tree[right(cur)].second;
            }
        }
    }

    SegmentTree(vector<int> &data) {
        input = data;
        len = data.size();
        tree.resize(len * 3);
        build(1, 0, len - 1);
    }

};


int main () {
    int n;
    cin >> n;
    vector<int> k(n);
    for(int i = 0; i < n; i++) {
        cin >> k[i];
    }
    SegmentTree st(k);
    pair<int, int> answer = st.rmq(3, 7);
    cout << answer.first << " " << answer.second<< "\n";


    return 0;
}