/*
ID: n.nekog1
LANG: C++
TASK: hamming
*/
#include <iostream>
#include <fstream>
#include <math.h>
#include <vector>

using namespace std;

int count (int a) {
    int count = 0;
    while(a) {
        count += a&1;
        a >>= 1;
    }
    return count;
}

int main () {
    ifstream in ("hamming.in");
    ofstream out ("hamming.out");
    int n, b, d, i = 1, tmp = 1;
    in >> n >> b >> d;
    vector<int> results;
    results.push_back(0);
    tmp <<= b;
    while(i < tmp) {
        int j = 0;
        if(results.size() == n)
            break;
        while(j < results.size() && count(results[j] ^ i) >= d)
            j++;
        if (j == results.size())
            results.push_back(i);
        i++;
    }

    for (int j = 0; j < results.size() - 1; j++) {
        if(j % 10 == 9)
            out << results[j] << "\n";
        else 
            out << results[j] << " ";
    }
    out << results[results.size() - 1] <<"\n";

    return 0;
}