    /*
ID: n.nekog1
LANG: C++
TASK: ride
*/
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main () {
    ifstream input ("ride.in");
    ofstream output ("ride.out");
    string first, second;
    input >> first >> second;
    int remainder1 = 1, remainder2 = 1;
    for(int i = 0; i < first.length(); i++) {
        remainder1 = ( remainder1 * int(first[i] - 'A' + 1) ) % 47;
    }
    cout <<endl;
    for(int i = 0; i < second.length(); i++) {
        remainder2 = ( remainder2 * int(second[i] - 'A' + 1) )% 47;
    }
    if(remainder1 == remainder2)
        output << "GO\n";
    else 
        output << "STAY\n";
    

    return 0;
}