#include <iostream>
#include <vector>
#include <stack>

using namespace std;

int main () {
    int n, count = 0;
    cin >> n;
    string a;
    cin >> a;

    stack<int> bracket;
    vector<bool> check(n, 0);

    // for (int i = 0; i < n - 1; i++) {
    //     // if((a[i] == '(' && !check[i]) && (a[i + 1] == ')' && !check[i + 1])) {
    //     if(a[i] == '(' && a[i + 1] == ')') {
    //         count ++;
    //         check[i] = 1;
    //         check[i + 1] = 1;
    //         i++;
    //     }
    // }
    for (int i = 0; i < n; i++) {
        if(i > 0 && a[i] == ')' && bracket.top() == '(') {
            bracket.pop();
            count ++;
        } else {
            bracket.push(a[i]);
        }
    }
    cout << bracket.empty() << " " << count << "\n";

    // vector<int> index;
    // for (int i = 0; i < n; i++) {
    //     if(!check[i])
    //         index.push_back(i);
    // }


    return 0;
}