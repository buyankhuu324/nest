#include <iostream>

using namespace std;

int main() {
    int limit = 10;
    int sum = 0, status = 0;
    for(; status < limit; status++) {
        if((status + 7) % 3 == 0)
            sum += status;
    }
    cout << sum << '\n';

    return 0;

}