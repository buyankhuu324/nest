/*
ID: n.nekog1
LANG: C++
TASK: castle
*/
#include <iostream>
#include <fstream>
#include <math.h>
#include <vector>

using namespace std;
vector<vector<int> > graph;
vector<vector<bool> > castle1(51, vector<bool> (50, 1) );
vector<vector<bool> > castle2(50, vector<bool> (51, 1) );
int m, n;
void draw (vector<vector<int> > a) {
    for(int i = 0; i < n; i++) {
        for(int j = 0; j < m; j++) {
            if((a[i][j] & 8) == 0)
                castle1[i + 1][j] = 0;
            if((a[i][j] & 2) == 0)
                castle1[i][j] = 0;
            if((a[i][j] & 4) == 0)
                castle2[i][j + 1] = 0;
            if((a[i][j] & 1) == 0)
                castle2[i][j] = 0;
        }
    }
}
int connection (int i, int j, int cur, int color) {
    graph[i][j] = color;
    for(int l = 0; l < 2; l++) {
        if(!castle1[i + l][j] && !graph[i + l * 2 - 1][j]) {
            cur += connection(i + l * 2 - 1, j, 1, color);
        }
        if(!castle2[i][j + l] && !graph[i][j + l * 2 - 1]) {
            cur += connection(i, j + l * 2 - 1, 1, color);
        }
    }
    return cur;
}

int main() {
    ifstream in ("castle.in");
    ofstream out ("castle.out");
    int best = 0, rooms = 0;
    in >> m >> n;
    graph.resize(n);
    vector<vector<int> > a (n, vector<int> (m));
    for(int i = 0; i < n; i++) {
        graph[i].resize(m);
        for(int j = 0; j < m; j++) {
            in >> a[i][j] ;
            graph[i][j] = 0;
        }
    }
    draw(a);
    for(int i = 0; i < n; i++) {
        for(int j = 0; j < m; j++) {
            if(!graph[i][j]) {
                rooms++;
                best = max(best, connection(i, j, 1, rooms));
            }
        }
    }
    cout << rooms << "\n" << best << "\n";

    

    return 0;

}