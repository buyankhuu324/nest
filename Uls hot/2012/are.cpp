#include <iostream>
#include <cmath>
#include <iomanip>
#define p 3.1415
using namespace std;

int main () {
    double x1, x2, y1, y2, r1, r2, area; 
    cin >> x1 >> y1 >> r1 >> x2 >> y2 >> r2;

    if(r1 < r2) {
        swap(x1, x2);
        swap(y1, y2);
        swap(r1, r2);
    }

    double l = sqrt(abs(x1 - x2) * abs(x1 - x2) + abs(y1 - y2) * abs(y1 - y2));
    if(l > r1 + r2) {
        cout << fixed << setprecision(2) << 0 << "\n";
        return 0;
    }
    if(l + r2 < r1) {
        area = r2 * r2 * p;
        cout << area << "\n";
    }
    double a, b, alpha;
    alpha = acos((r1* r1 + r2 * r2 - l * l) / 2 * r1 * r2);
    a = cos(abs(alpha - 90)) * r2;
    b = sin(abs(alpha - 90)) * r2;
    

    return 0;
}