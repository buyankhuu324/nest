/*
ID: n.nekog1
LANG: C++
TASK: lamps
*/
#include <iostream>
#include <vector>
#include <fstream>
#include <unordered_map>
#include <algorithm>

using namespace std;

vector<int> muston, mustoff;
unordered_map<int, vector<bool> > memo;
vector<vector<bool> > results;

vector<bool> button1 (vector<bool> lamps) {
    for(int i = 0; i < lamps.size(); i++)
        lamps[i] = !lamps[i];
    return lamps;
}
vector<bool> button2 (vector<bool> lamps) {
    for(int i = 0; i < lamps.size(); i+=2) 
        lamps[i] = !lamps[i]; 
    return lamps;
}
vector<bool> button3 (vector<bool> lamps) {
    for(int i = 1; i < lamps.size(); i+=2) 
        lamps[i] = !lamps[i];
    return lamps;
}
vector<bool> button4 (vector<bool> lamps) {
    for(int i = 0; i < lamps.size(); i+=3)
        lamps[i] = !lamps[i];
    return lamps;
}
void interrupt (vector<bool> a, int n, int key) {
    if(n == 0) {
        for(int i = 0; i < muston.size(); i++)
            if(!a[muston[i] - 1])
                return;
        for(int i = 0; i < mustoff.size(); i++)
            if(a[mustoff[i] - 1])
                return;
        for(int i = 0; i < results.size(); i++) 
            if(a == results[i])
                return; 
        results.push_back(a);
        return;
    }
    if(memo.find(key + 1000) == memo.end()) {
        memo[key + 1000] = button1(a);
        interrupt(button1(a), n - 1, key + 1000);
    }
    if(memo.find(key + 100) == memo.end()) {
        memo[key + 100] = button2(a);
        interrupt(button2(a), n - 1, key + 100);
    }
    if(memo.find(key + 10) == memo.end()) {
        memo[key + 10] = button3(a);
        interrupt(button3(a), n - 1, key + 10);
    }
    if(memo.find(key + 1) == memo.end()) {
        memo[key + 1] = button4(a);
        interrupt(button4(a), n - 1, key + 1);
    }

}



int main () {
    ifstream in ("lamps.in");
    ofstream out ("lamps.out");
    int n, c;
    in >> n >> c;
    int k;
    do{
        in >> k;
        muston.push_back(k);
    } while(k != -1); 
    muston.pop_back();
    do{
        in >> k;
        mustoff.push_back(k);  
    } while(k != -1);
    mustoff.pop_back();
    vector<bool> lamps(n, 1);
    interrupt(lamps, c, 0);
    //sorting
    sort(results.begin(), results.end(), [](vector<bool> a, vector<bool> b) {
        for (int i = 0; i < a.size(); i++) {
            if (!a[i] && b[i]) return true;
            if (a[i] && !b[i]) return false;
        }
        return false;
    });


    if(results.size() != 0)
        for(int i = 0; i < results.size(); i++)
            for(int j = 0; j < n; j++) {
                if(j == n - 1)
                    out << results[i][j] << "\n";
                else 
                    out << results[i][j];
            }
    else 
        out << "IMPOSSIBLE\n";
    return 0;
}