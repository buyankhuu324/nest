/*
ID: n.nekog1
LANG: C++
TASK: gift1
*/
#include <iostream>
#include <fstream>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;

int main() {
    unordered_map<string, int> friends;
    vector<string> str;
    
    ifstream input ("gift1.in");
    ofstream output ("gift1.out");
    int n;
    input >> n;
    string name;
    for(int i = 0; i < n; i++) {
        input >> name;
        str.push_back(name);
        friends[name] = 0;
    }

    for(int i = 0; i < n; i++) {
        int coin, numbers;
        string owner;
        input >> owner >> coin >> numbers;
        if(numbers != 0) {
            friends[owner] -= coin;
            friends[owner] += (coin % numbers);
        }
        vector<string> names;
        for(int i = 0; i < numbers; i++) {
            input >> name;
            names.push_back(name);
        }
        for(int i = 0; i < numbers; i++)
            friends[names[i]] += (coin / numbers);
    }
    for(int i = 0; i < n; i++) {
        output << str[i] << " " << friends[str[i]] << "\n";
    }


    return 0;
}