/*
ID: n.nekog1
LANG: C++
TASK: preface
*/

#include <iostream>
#include <fstream>
#include <vector>
#include <unordered_map>

using namespace std;

unordered_map<int, int> count;

void calc(int n) {
    int common = 1;
    while(n != 0) {
        int tmp = n % 10;
        if(tmp == 4 || tmp == 9) {
            count[common * (tmp / 5 + 1) * 5] ++;
            count[common] ++;
        } else if (tmp < 5) {
            count[common] += tmp;
        } else {
            count[common * 5] ++;
            count[common] += tmp % 5;
        }
        n /= 10;
        common *= 10;
    }
}

int main () {
    ifstream in("preface.in");
    ofstream out("preface.out");
    string str = "IVXLCDM";
    unordered_map<int, char> horizontal;
    int n, k = 1;
    in >> n;
    for (int i = 0; i < 7; i++) {
        horizontal[k] = str[i]; 
        if(i % 2 == 0)
            k *= 5;
        else 
            k *= 2;
    }
    for (int i = 1; i <= n; i++) {
        calc(i);
    }

    k = 1;
    for (int i = 0; i < 7; i++) {
        if(count[k] != 0)
            out << horizontal[k] << " " << count[k] << '\n';
        if(i % 2 == 0)
            k *= 5;
        else 
            k *= 2;
    }
    

    return 0;
}