#include <iostream>

using namespace std;

void solve(long long a, long long b) {
    if(a >= b) {
        cout << "yes\n";
        return;
    }
    if(a == 1) {
        cout << "no\n";
        return;
    }
    if(a == 3 || (a == 2 && b != 3)) {
        cout << "No\n";
        return ;
    }
    cout << "Yes\n";
}

int main () {
    long long t;
    cin >> t;

    for(long long i = 0; i < t; i++) {
        long long a, b;
        cin >> a >> b;
        solve(a, b);
    }

    return 0;
}