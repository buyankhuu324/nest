#include <iostream>
#include <vector>

using namespace std;

struct Stree {
    pair<int, int> val;
    Stree *left;
    Stree *right;
    pair<int, int> param;
    // Stree *adress;
    Stree() {
        // cout << root << " \n";
        left = NULL;
        right = NULL;
        cout << "here\n";
    }
    void build (int l, int r, vector<int> &input) {
        param.first = l;
        param.second = r;
        
        if(l == r) {
            val.first = l;
            val.second = input[l];
            return;
        }
        Stree *nl = new Stree();
        Stree *nr = new Stree();
        cout << "how do you do? \n";
        left = nl;
        right = nr;

        nl->build(l, (r + l) / 2, input);
        nr->build((l + r) / 2 + 1, r, input);

        if(left->val.second > right->val.second)
            val = right->val;
        else 
            val = left->val;

    }
    void build(vector<int> &input) {
        cout << "here I am\n";
        build(0, input.size() - 1, input);
    }
    pair<int, int> rmq (int i, int j) {
        if(param.first > j || param.second < i)
            return make_pair(-1, -1);

        if(param.first < i && param.second > j)
            return val;
        
        pair<int, int> l = left->rmq(i, j);
        pair<int, int> r = right->rmq(i, j);
        
        if(l.first == -1) 
            return r;
        if(r.first == -1)
            return l;
        if(l.second < r.second) 
            return l;
        return r;
    }
};

int main () {
    int n;
    cin >> n;
    // cout << "nani\n";
    vector<int> input(n);
    for(int i = 0; i < n; i++)
        cin >> input[i];
    Stree tmp;
    Stree *root = &tmp;

    root->build(input);
    int k;
    cin >> k;
    for(int i = 0; i < k; i++) {
        int l, r;
        cin >> l >> r;
        cout << root->rmq(l, r).first << " indeced " << root->rmq(l, r).second << "\n";
    }

    return 0;
}