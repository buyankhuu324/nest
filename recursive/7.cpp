#include <iostream>
#include <vector>

using namespace std;

int rec(int max, vector<int>& a, int ind) {
    if(ind == a.size())
        return max;
    if(a[ind] > a[max])
        max = ind;
    return rec(max, a, ind + 1);
}

int main() {
    int n;

    cin >> n;

    vector<int> a(n);

    for(int i = 0; i < n; i++)
        cin >> a[i];

    cout << rec(0, a, 0) << "\n";

    return 0;
}