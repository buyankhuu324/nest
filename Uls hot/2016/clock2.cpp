#include <iostream>
#include <string>
using namespace std;

int main () {
    freopen ("clock2.in", "r", stdin);
    int count = 0, hour, minute;
    string a;
    while(getline(cin, a)) {
        // cout << a << " \n";
        hour = int(a[0] - '0');
        hour = hour * 10 + int(a[1] - '0');
        hour %= 12;
        minute = int(a[3] - '0');
        minute = minute * 10 + int(a[4] - '0');
        hour *= 5;
        hour += (minute / 12);
        if(abs(hour - minute) == 15) count++;
    }
    cout << count << "\n";

    return 0;
}