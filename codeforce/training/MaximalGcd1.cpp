#include <iostream>
#include <vector>
#include <math.h>

using namespace std;
#define ull unsigned long long

vector<ull> finddivisor(ull n, ull k) {
    vector<ull> ar1;
    vector<ull> ar2;
    for (int i = 1; i <= sqrt(n); i++) {
        if (n % i == 0) {
            if(n / i == i) 
                ar2.push_back(i);
            else {
                ar1.push_back(i);
                ar2.push_back(n / i);
            }
        }
    }
    for (int i = ar1.size() - 1; i >= 0; i--)
        ar2.push_back(ar1[i]);    
    return ar2;
}
int main () {
    ull n, k;
    cin >> n >> k;
    if (k == 1) {
        cout << n << "\n";
        return 0;
    }
    if (n/k < (k + 1) / 2 || n / (k + 1) < k / 2) {
        cout << -1 << "\n";
        return 0;
    }
    vector<ull> divisors = finddivisor(n, k);
    int i = 0;
    ull tmp = n / divisors[i];
    while (tmp < (k * (k + 1) / 2) && (i < divisors.size() - 1)) {
        i++;
        tmp = n / divisors[i];
    }
    for (int j = 1; j < k; j++) {
        cout << divisors[i] * j << " ";
    }
    cout << divisors[i] * (tmp - (k * (k - 1) / 2)) << "\n"; 
    return 0;
}