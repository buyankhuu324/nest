#include <iostream>
#include <vector>

using namespace std;

bool rec(int min, vector<int>& a, int ind) {
    if(ind == a.size())
        return min ;
    if(a[ind] < min)
        min = a[ind];
    // return rec(max, a, ind + 1);
}

int main () {
    int n;
    cin >> n;
    vector<int> a(n);

    for(int i = 0; i < n; i++)
        cin >> a[i];

    // rec(a, 0, a[0]);

    return 0;
}