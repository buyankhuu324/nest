#include<iostream>
#include<fstream>
#include<vector>

using namespace std;
int main(){
    
    // freopen("input.txt", "r",stdin);
    // freopen("output.txt", "w",stdout);
    
    int n, m, d;

    cin >> n >> m >> d;
    int wood[m + 1];
    int road[2000];
    int sum = 0;
    for (int i = 0; i < m; i++){
        cin >> wood[i];
        sum += wood[i];
    }
    int water = n - sum;
    if (water < 0 || (water != 0 && d == 1)){
        cout << "NO" << endl;
    }else if (water == 0) {
        cout << "YES" << endl;
        for (int i = 0; i < m - 1; i++){
            for (int j = 0; j < wood[i]; j++){
                cout << i + 1 << " ";
            }
        }
        for (int j = 0; j < wood[m - 1] - 1; j++)
            cout << m << " ";
        cout << m << endl;
    } else if ( (water - 1)/(d - 1) > m){
        cout << "NO" << endl;
    } else{
        int split = (water - 1) / (d - 1) + 1;
        int min_water = water % (d - 1);
        if (min_water == 0)
            min_water = d - 1;    
        cout << "YES" << endl;

        //cout << "YES" << ' ' << water << ' ' << split << ' ' << min_water << endl;

        int idx = 0;
        for (int i = 1; i < split; i++){
            for (int j = 1; j < d; j++){
                road[idx] = 0;
                idx++;
            }
            for (int j = 0; j < wood[i - 1]; j++){
                road[idx] = i;
                idx++;
            }
        }
        for (int i = split; i <= m; i++){
            for (int j = 0; j < wood[i - 1]; j++){
                road[idx] = i;
                idx++;
            }
        }
        for (int j = 0; j < min_water; j++){
                road[idx] = 0;
                idx++;
        }
        for (int i = 0; i < n; i++){
            cout << road[i] << ' ';
        }
        cout << endl;
    }
    return 0;
}


