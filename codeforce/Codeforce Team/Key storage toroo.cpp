#include <iostream>
#include <vector>

using namespace std;

vector<int> makevector(vector<int>& uld, long long n) {
    int p = 2;
    while (n > 0) {
        uld.push_back(n % p);
        n /= p;
        p++;
    }
    return uld;
}

long long solve(long long n) {
    vector<int> uld;
    makevector(uld, n);
    
}

int main() {
    int t;
    cin >> t;
    
    while (t--) {
        long long n;
        cin >> n;
        cout << solve(n) << endl;
    }
}