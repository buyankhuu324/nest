#include <iostream>
#include <vector>
#include <unordered_map>
#include <algorithm>

using namespace std;

void solve() {
    int n, m;
    cin >> n >> m;
    int temp = m;
    vector<pair<int, int> > a(n);
    int sum = 0;
    vector<pair<int, int> > result;
    for(int i = 0; i < n - 1; i++) {
        cin >> a[i].first;
        a[i].second = i + 1;
        sum += a[i].first;
        m--;
        result.push_back(make_pair(i + 1, (i + 2)));
    }
    cin >> a[n - 1].first;
    a[n - 1].second = n;
    if(n > temp || n == 2) {
        cout << -1 << "\n";
        return;
    }
    
    result.push_back(make_pair(n , 1));
    sum += a[n - 1].first;
    m--;
    sum *= 2;
    sort(a.begin(), a.end());
    for(int i = 0; i < m; i++) {
        sum += ((a[0].first + a[1].first));
        result.push_back(make_pair(a[0].second, a[1].second));
    }
    cout << sum << "\n";
    for(int i = 0; i < result.size(); i++) {
        cout << result[i].first << " " << result[i].second << "\n";
    }
    return;
}

int main () {
    int t;
    cin >> t;

    for(int i = 0; i < t; i++) {
        solve();
    }

    return 0;
}