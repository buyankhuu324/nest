#include <iostream>
#include <vector>
#include <algorithm>
#define ull unsigned long long
using namespace std;
 
pair<int, int> unpack(ull n) {
    int five = 0, two = 0;
    while(n % 5 == 0 || n % 2 == 0) {
        if(n % 5 == 0) {
            five++;
            n /= 5;
        }
        if(n % 2 == 0) {
            two++;
            n /= 2;
        }
    }
    return make_pair(five, two);
}

int main () {
    int n, k;
    cin >> n >> k;
    vector<ull> a(n);
    vector<pair<int, int> > kor(n);
    //simplified
    for(int i = 0; i < n; i++) {
        cin >> a[i];
        kor[i] = unpack(a[i]);
    }
    //maximum range of dp
    sort(kor.begin(), kor.end());
    int sum = 0, sum2 = 0;
    for(int i = 0; i < n; i++) {
        sum += kor[i].first;
        sum2 += kor[i].second;
    }
    //my dp sizes
    vector<int> dpsizes(sum + 1, 0);
    for(int i = 0; i < n; i++) {
        dpsizes[kor[i].first]++;
    }
    for(int i = 1; i < sum + 1; i++)
        dpsizes[i] += dpsizes[i - 1];
    //real dp goes from here
    vector<int> results(sum + 1);
    vector<vector<vector<int> > > dp(sum + 1);
    for(int i = 0; i < sum + 1; i++) {
        dp[i].resize(dpsizes[i], vector<int> (k, -1));
        for(int j = 0; j < dp[i].size(); j++) {
            for(int col = 0; col < k && col <= j; col++) {
                int len = dp[i - kor[j].first].size() - 1;
                if(col == 0) {
                    if(kor[j].first == i)
                        dp[i][j][0] = kor[j].second;
                } else if(len < j) {
                    if(len >= 0 && dp[i - kor[j].first][len][col - 1] != -1)
                        dp[i][j][col] = dp[i - kor[j].first][len][col - 1] + kor[j].second;
                } else {
                    if(j != 0 && dp[i - kor[j].first][j - 1][col - 1] != -1)
                        dp[i][j][col] = dp[i - kor[j].first][j - 1][col - 1] + kor[j].second;
                }
            }
        }
        int temp = -1;
        for(int j = 0; j < dp[i].size(); j++) {
            temp = max(dp[i][j][k - 1], temp);
        }
        results[i] = min(i, temp);
    }
    cout << *max_element(results.begin(), results.end()) << "\n";
 
    return 0;
}