#include <iostream>
#include <vector>
using namespace std;

int partition(vector<int>& arr, int l, int r) {
    int i = l, x = arr[r];
    for(int j = l; j < r; j++) {
        if(x >= arr[j]) {
            swap(arr[i], arr[j]);
            i++;
        }
    }
    swap(arr[i], arr[r]);
    return i;
}

int kthlowest(vector<int>& arr, int l, int r, int k) {
    int index = partition(arr, l, r);
    // cout << index <<"\n";
    if(index == k)
        return arr[index];
    
    if(index < k)
        return kthlowest(arr, index + 1, r, k);

    return kthlowest(arr, l, index - 1, k);
}

int main () {
    int n, k;
    cin >> n;
    vector<int> input(n);

    for(int i = 0; i < n; i++)
        cin >> input[i];

    cin >> k;
    k--;

    cout << kthlowest(input, 0, n - 1, k) << '\n';
    // cout << partition(input, 0, n - 1) << "index \n";
    // for(int i = 0; i < n; i++)
    //     cout << input[i] << ' ';
    // cout << '\n';
    return 0;
}